<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/stylesheet.css">
    <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
    <title>YouFlex</title>
</head>
<body>
    <div class="title">
        <a href="index.php">
            <h3>YouFlex</h3>
        </a>
    </div>

    <div class="container-connection">        
        <div class="container-left">
            <h2>Inscris toi !</h2>
            <form action="forms/register.php" method="post">
                <input type="text" class="form-control" name="prenom" placeholder="Prénom" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                <input type="text" class="form-control" name="nom" placeholder="Nom" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                <input type="text" class="form-control" name="pseudo" placeholder="Pseudo" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                <input type="text" class="form-control" name="password" placeholder="Mot de passe" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                <input type="submit" class="form-control co-sub" aria-label="Small" aria-describedby="inputGroup-sizing-sm">     
            </form>
        </div>
        <div class="container-center"></div>
        <div class="container-right">
            <h2>Connecte toi !</h2>
            <form action="forms/login.php" method="post">
                    <input type="text" class="form-control" name="pseudo" placeholder="Pseudo" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                    <input type="text" class="form-control" name="password" placeholder="Mot de passe" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                    <input type="submit" class="form-control co-sub" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
            </form>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
</body>
</html>