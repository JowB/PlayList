<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/stylesheet.css">
    <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
    <title>YouFlex</title>
</head>
<body>
    <?php
        include(__DIR__ . "/classes/db.php");
    ?>
    <div class="title">
        <h3>YouFlex</h3>
        <a class="connection" href="connection.php">Inscription/Connection</a>
    </div>
    <div class="container">
        <div class="video">
            <iframe class="video-ifr" src="https://www.youtube.com/embed/RxabLA7UQ9k" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </div>
        <div class="containerLien">            
        <form action="">
        <?php
                //      Récupère les liens en BDD et affiche
            $bdd = Db::connection();
            $request = $bdd->query('SELECT * FROM playlist');
            $playlist = $request->fetchAll();

            foreach($playlist as $playlistArray){
        ?>
        <div>
            <input class="first-input" type="image" src="assets/play-button.png">
            <input class="second-input" type="text" <?php echo 'value="'. $playlistArray['lien'] .'"'?>>
            <input class="third-input" type="image" src="assets/delete-button.png">
        </div>
        <?php
            }
        ?>
        </form>
        </div>
        <div class="containerAdd">
            <form action="forms/addLien.php" method="post">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" name="url" placeholder="Balance ton son !" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <input class="btn btn-outline-secondary" type="submit" name="add"></button>
                        </div>
                </div>
            </form>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
</body>
</html> 





       <!-- <form action="forms/addLien.php" method="post">
        <div class="input-group mb-3">
            <input type="text" class="form-control" name="lien" placeholder="Balance ton son !" aria-label="Recipient's username" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <input class="btn btn-outline-secondary" src="assets/plus-button.svg"type="image" value="submit" name="add">Ajoutez</button>
                </div>
        </div> -->