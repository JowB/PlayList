<?php
    include(__DIR__ . "/../classes/db.php");

    //      Défini les variables login et password
    $login = $_POST['login'];
    $password = $_POST['password'];

    //      Fonction login de la class Db
    Db::login($login, $password);

    header('Location: ../index.php');
?>