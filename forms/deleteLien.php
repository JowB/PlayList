<?php
    include('../classes/db.php');

    //      Défini la variable id
    $id = $_POST['id'];

    //      Suppression du lien sélectionné grâce à son id
    Db::deleteLien($id);

    header('Location: ' . $_SERVER['HTTP_REFERER']);
?>