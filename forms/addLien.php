<?php
    include("../classes/db.php");

    //      Défini la variable lien
    $url = $_POST['url'];

    //      Ajout du lien dans la BDD
    Db::addLien($url);

    header('Location: ' . $_SERVER['HTTP_REFERER']);
?>