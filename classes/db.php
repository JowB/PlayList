<?php
    class Db{
        
        //      Lien avec la BDD
        static function connection(){
            $user = 'root';
            $password = 'root';
            $host = 'localhost' ;
            $database = 'PlayList';
            $bdd = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $password);
            $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $bdd;
        }

        //      Ajoute un lien à la BDD
        static function addLien($url){
            $bdd = self::connection();
            $request = $bdd->prepare("INSERT INTO playlist VALUES(NULL,:lien)");
            $request->execute(['lien' => $url]);
        }

        //      Supprime un lien de la BDD
        static function deleteLien($id){
            $bdd = self::connection();
            $request = $bdd->prepare('DELETE FROM playlist WHERE id = :id');
            $response = $request->execute(['id' => $id]);
        }

        //      Permet de se connecter
        static function login($login, $password){
            $bdd = self::connection();
            $request = $bdd->prepare('SELECT id FROM utilisateurs WHERE pseudo=:login AND mot_de_passe=:mot_de_passe');
            $response = $request->execute(['login' => $login, 'mot_de_passe' => $password]);
            return $response;
        }

        //      Permet de s'inscrire
        static function register($prenom, $nom, $pseudo, $password){
            $bdd = self::connection();
            $request = $bdd->prepare('INSERT INTO utilisateurs VALUES(NULL, :prenom, :nom, :pseudo, :mot_de_passe)');
            $request->execute(['prenom' => $prenom, 'nom' => $nom, 'pseudo' => $pseudo, 'mot_de_passe' => $password]);
        }
    }
    